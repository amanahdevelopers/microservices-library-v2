﻿using System;
using System.Collections.Generic;
using System.Text;
using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Mvc;
using DispatchProduct.Controllers;
using AutoMapper;
using Utilites.ProcessingResult;
using Microsoft.Extensions.Options;
using Utilites.UploadFile;
using Utilites.PaginatedItemsViewModel;
using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System.Linq.Expressions;

namespace DispatchProduct.Controllers.V1
{

    public class BaseControllerV1<ITManager, T, TViewModel> : BaseController<ITManager, T, TViewModel>
       where ITManager : IRepository<T>
        where TViewModel : class, IRepoistryBaseEntity
        where T : class, IBaseEntity
    {
        public BaseControllerV1(ITManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {

        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get/{id}")]
        //[MapToApiVersion("1.0")]
        [HttpGet]
        public new virtual ProcessResultViewModel<TViewModel> Get([FromRoute]int id)
        {
            return base.Get(id);
        }

        [Route("Count")]
        //[MapToApiVersion("1.0")]
        [HttpGet]
        public new virtual ProcessResultViewModel<int> Count()
        {
            return base.Count();
        }

        [Route("GetByIds")]
        //[MapToApiVersion("1.0")]
        [HttpPost]
        public new virtual ProcessResultViewModel<List<TViewModel>> GetByIds([FromBody]List<int> ids)
        {
            return base.GetByIds(ids);
        }

        //[MapToApiVersion("1.0")]
        [Route("GetAll")]
        [HttpGet]
        public new virtual ProcessResultViewModel<List<TViewModel>> Get()
        {
            return base.Get();
        }

        //[MapToApiVersion("1.0")]
        [Route("GetAllPaginatedByPredicate")]
        [HttpPost]
        public new virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginatedByPredicate([FromBody]PaginatedItemsViewModel<TViewModel> model = null, Expression<Func<T, bool>> predicate = null)
        {
            return base.GetAllPaginatedByPredicate(model, predicate);
        }

        //[MapToApiVersion("1.0")]
        [Route("GetAllPaginated")]
        [HttpPost]
        public new virtual ProcessResultViewModel<PaginatedItemsViewModel<TViewModel>> GetAllPaginated([FromBody]PaginatedItemsViewModel<TViewModel> model = null)
        {
            return base.GetAllPaginated(model);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPost]
        //[MapToApiVersion("1.0")]
        [Route("Add")]
        public new virtual ProcessResultViewModel<TViewModel> Post([FromBody]TViewModel model)
        {
            return base.Post(model);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [HttpPut]
        //[MapToApiVersion("1.0")]
        [Route("Update")]
        public new virtual ProcessResultViewModel<bool> Put([FromBody]TViewModel model)
        {
            return base.Put(model);
        }
        #endregion
        
        #region DeleteApi
        [HttpDelete]
        //[MapToApiVersion("1.0")]
        [Route("Delete/{id}")]
        public new virtual ProcessResultViewModel<bool> Delete([FromRoute]int id)
        {
            return base.Delete(id);
        }
        #endregion

        #endregion
    }
}

