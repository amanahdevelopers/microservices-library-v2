﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum DeviceType
    {
        Mobile = 1,
        Web = 2
    }
}
