﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CommonEnums
{
    public static class EnumManager<T> where T : struct, IConvertible
    {
        public static List<EnumEntity> GetEnumList()
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new InvalidOperationException();
            List<EnumEntity> lst = new List<EnumEntity>();
            foreach (var e in Enum.GetValues(typeof(T)))
            {
                var fi = e.GetType().GetField(e.ToString());
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0 && attributes[0].Description != null)
                {
                    lst.Add(new EnumEntity { Name = attributes[0].Description, Id = (int)e });
                }
                else
                {
                    lst.Add(new EnumEntity { Name = e.ToString(), Id = (int)e });
                }
            }
            return lst;
        }
        public static List<EnumEntity> GetEnumObjects(List<T> enumValues)
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new InvalidOperationException();
            List<EnumEntity> lst = new List<EnumEntity>();
            foreach (var e in enumValues)
            {
                var fi = e.GetType().GetField(e.ToString());
                if (fi != null)
                {
                    var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    if (attributes.Length > 0 && attributes[0].Description != null)
                    {
                        lst.Add(new EnumEntity { Name = attributes[0].Description, Id = Convert.ToInt32(e) });
                    }
                    else
                    {
                        lst.Add(new EnumEntity { Name = e.ToString(), Id = Convert.ToInt32(e) });
                    }
                }

            }
            return lst;
        }
        public static string GetName(T obj)
        {
            return Enum.GetName(typeof(T), obj);
        }
        public static string GetDescription(T obj)
        {
            var fi = obj.GetType().GetField(obj.ToString());
            if (fi != null)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Length > 0 && attributes[0].Description != null)
                {
                    return attributes[0].Description;
                }
            }
            return Enum.GetName(typeof(T), obj);
        }
        public static T GetEnumObject(string name)
        {
            object data;
            T result ;
            Enum.TryParse(typeof(T), name, out data);
            if (data != null && data is T)
            {
                result = (T)data;
            }
            else
            {
                result = new T();
            }
            return result;
        }
        public static EnumEntity GetEnumObject(T e)
        {
            var type = typeof(T);
            if (!type.IsEnum)
                throw new InvalidOperationException();
               EnumEntity obj = new EnumEntity ();
                var fi = e.GetType().GetField(e.ToString());
                if (fi != null)
                {
                    var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    if (attributes.Length > 0 && attributes[0].Description != null)
                    {
                    obj =  new EnumEntity { Name = attributes[0].Description, Id = Convert.ToInt32(e) };
                    }
                    else
                    {
                    obj = new EnumEntity { Name = e.ToString(), Id = Convert.ToInt32(e) };
                    }
                }
            return obj;
        }
    }

    public class EnumEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }


}
