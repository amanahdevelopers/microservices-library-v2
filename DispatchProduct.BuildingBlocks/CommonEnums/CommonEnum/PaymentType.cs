﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum PaymentType
    {
       KuwaitCashOnDelivery =1,
       UAECashOnDelivery = 2,
       KNet=3,
       MasterCard=4,
       Wallet=5
    }
}
