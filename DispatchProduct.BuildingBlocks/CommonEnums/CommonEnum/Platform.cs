﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnums
{
    public enum Platform
    {
        MobileAndroid=1,
        MobileIOS=2,
        WebStore=3,
        WebCRM= 4
    }
}
