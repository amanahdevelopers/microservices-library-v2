﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum TechnicianState
    {
        Working = 1,
        Idle = 2,
        Break = 3,
        End = 4
    }
}
