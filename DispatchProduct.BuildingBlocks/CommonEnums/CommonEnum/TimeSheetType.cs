﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonEnum
{
    public enum TimeSheetType
    {
        Actual = 1,
        Traveling = 2,
        Idle = 3,
        Break = 4
    }
}
