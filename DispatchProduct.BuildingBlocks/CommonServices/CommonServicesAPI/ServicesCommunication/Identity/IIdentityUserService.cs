﻿using CommonServicesAPI.ServicesSettings.Identity;
using CommonServicesAPI.ServicesViewModels.Identity;
using DispatchProduct.HttpClient;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace CommonServicesAPI.ServicesCommunication.Identity
{
    public interface IIdentityUserService
       : IDefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>
    {
        Task<ProcessResultViewModel<ApplicationUserViewModel>> AddCustomerUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> UpdateCustomerUser(string version, ApplicationUserViewModel user);
        Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string version, string userId);
        Task DeleteById(string version, string userId);
    }
}
