﻿using BuildingBlocks.ServiceDiscovery;
using DispatchProduct.HttpClient;
using DnsClient;
using System.Threading.Tasks;

namespace CommonServicesAPI.ServicesSettings.Identity
{
    public class IdentityServiceSettings : DefaultHttpClientSettings
    {
        private new string Uri { get => base.Uri; set => base.Uri = value; }

        public string AddUserAction { get; set; }
        public string UpdateUserAction { get; set; }
        public string GetUserById { get; set; }
        public string DeleteUserById { get; set; }

        public async Task<string> GetBaseUrl(IDnsQuery dnsQuery)
        {
            return await ServiceDiscoveryHelper.GetServiceUrl(dnsQuery, ServiceName);
        }
    }
}
