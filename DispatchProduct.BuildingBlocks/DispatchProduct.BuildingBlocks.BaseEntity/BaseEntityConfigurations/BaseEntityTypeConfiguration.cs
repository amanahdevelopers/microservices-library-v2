﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations
{
    public class BaseEntityTypeConfiguration<T>
        : IdentityBaseEntityTypeConfiguration<T> where T : class, IBaseEntity
    {
        public  virtual void Configure(EntityTypeBuilder<T> BaseEntityConfiguration)
        {
            BaseEntityConfiguration.HasKey(o => o.Id);
            BaseEntityConfiguration.Ignore(o => o.CurrentUserId);
        }
    }
}
