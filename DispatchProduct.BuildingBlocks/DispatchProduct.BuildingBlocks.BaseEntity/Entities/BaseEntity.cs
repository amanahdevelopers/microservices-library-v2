﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.Entities
{
    public class BaseEntity : IdentityBaseEntity, IBaseEntity
    {
        public int Id { get; set; }
        public string CurrentUserId { get; set; }
    }
}
