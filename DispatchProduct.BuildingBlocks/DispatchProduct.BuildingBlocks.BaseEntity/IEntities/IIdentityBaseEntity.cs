﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.IEntities
{
    public interface IIdentityBaseEntity: ILogicalDeletingBaseEntity
    {
          string FK_CreatedBy_Id { get; set; }

          string FK_UpdatedBy_Id { get; set; }

          string FK_DeletedBy_Id { get; set; }

          DateTime CreatedDate { get; set; }

          DateTime UpdatedDate { get; set; }

          DateTime DeletedDate { get; set; }

    }
}
