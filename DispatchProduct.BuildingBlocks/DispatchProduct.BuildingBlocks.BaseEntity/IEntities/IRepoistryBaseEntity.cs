﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchProduct.BuildingBlocks.BaseEntities.IEntities
{
    public interface IRepoistryBaseEntity
    {
        //bool IsDeleted { get; set; }
        int Id { get; set; }
        string CurrentUserId { get; set; }

    }
}
