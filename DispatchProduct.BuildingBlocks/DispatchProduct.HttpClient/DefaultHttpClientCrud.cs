﻿using DispatchProduct.Api.HttpClient;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace DispatchProduct.HttpClient
{
    public class DefaultHttpClientCrud<T, TIn, TOut> : IDefaultHttpClientCrud<T, TIn, TOut> where T : IHttpClientSettings
    {
        T _obj;
        IDnsQuery _dnsQuery;
        public DefaultHttpClientCrud(T obj)
        {
            _obj = obj;
        }
        public DefaultHttpClientCrud(T obj, IDnsQuery dnsQuery)
        {
            _obj = obj;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<List<TOut>>> GetList(string auth = "")
        {
            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.GetAllVerb}";
            var response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<ProcessResultViewModel<List<TOut>>>();
            return result;
        }
        public async Task<ProcessResultViewModel<List<TOut>>> GetList(string requestUri, string auth = "")
        {
            var response = await HttpRequestFactory.Get(requestUri, auth);
            var result = response.ContentAsType<ProcessResultViewModel<List<TOut>>>();
            return result;
        }
        public async Task<ProcessResultViewModel<List<TOutCustomize>>> GetListCustomized<TOutCustomize>(string requestUri, string auth = "")
        {
            var response = await HttpRequestFactory.Get(requestUri, auth);
            var result = response.ContentAsType<ProcessResultViewModel<List<TOutCustomize>>>();
            return result;
        }

        public async Task<ProcessResultViewModel<TOut>> GetItem(string id, string auth = "")
        {
            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.GetVerb}/{id}";
            return await GetByUri(requesturi, auth);
        }
        public async Task<ProcessResultViewModel<TOut>> Get(string requesturi, string auth = "")
        {
            return await GetByUri(requesturi, auth);
        }

        public async Task<ProcessResultViewModel<TOutCustomize>> GetCustomized<TOutCustomize>(string requesturi, string auth = "")
        {
            return await GetByUriCustomized<TOutCustomize>(requesturi, auth);
        }

        public async Task<ProcessResultViewModel<List<TOut>>> GetByIds<TInCustomize>(TInCustomize model, string auth = "")
        {
            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.GetByIdsVerb}";
            return await GetByIds(requesturi, model, auth);
        }
        public async Task<ProcessResultViewModel<List<TOut>>> GetByIds<TInCustomize>(string requestUri, TInCustomize model, string auth = "")
        {
            return await PostCustomize<TInCustomize, List<TOut>>(requestUri, model, auth);
        }

        public async Task<ProcessResultViewModel<TOut>> Post(TIn model, string auth = "")
        {
            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.PostVerb}";
            return await Post(requesturi, model, auth);
        }
        public async Task<ProcessResultViewModel<TOut>> Post(string requesturi, TIn model, string auth = "")
        {
            var response = await HttpRequestFactory.Post(requesturi, model, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOut>>();
            return result;
        }

        public async Task<ProcessResultViewModel<TOutCustomize>> PostCustomize<TInCustomize, TOutCustomize>(string requesturi, TInCustomize model, string auth = "")
        {
            var response = await HttpRequestFactory.Post(requesturi, model, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOutCustomize>>();
            return result;
        }
        
        public async Task<ProcessResultViewModel<TOut>> Put(TIn model, string id, string auth = "")
        {
            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.PutVerb}/{id}";
            return await Put(requesturi, model, auth);
        }
        public async Task<ProcessResultViewModel<TOut>> Put(string requesturi, TIn model, string auth = "")
        {
            var response = await HttpRequestFactory.Put(requesturi, model, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOut>>();
            return result;
        }
        public async Task<ProcessResultViewModel<bool>> Put(string requesturi, string auth = "")
        {
            var response = await HttpRequestFactory.Put(requesturi, null, auth);
            var result = response.ContentAsType<ProcessResultViewModel<bool>>();
            return result;
        } 

        public async Task<ProcessResultViewModel<TOutCustomize>> PutCustomize<TInCustomize, TOutCustomize>(string requesturi, TInCustomize model, string id, string auth = "")
        {

            var response = await HttpRequestFactory.Put(requesturi, model, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOutCustomize>>();
            return result;
        }

        public async Task Patch(string id, string auth = "")
        {
            var model = new[]
            {
                new
                {
                    op = "replace",
                    path = "/title",
                    value = "Thunderball-Patch"
                }
            };

            var requesturi = $"{await _obj.GetUri(_dnsQuery)}/{_obj.PatchVerb}/{id}";
            var response = await HttpRequestFactory.Patch(requesturi, model, auth);
        }

        public async Task Delete(string id, string auth = "")
        {
            var uri = $"{await _obj.GetUri(_dnsQuery)}/{_obj.DeleteVerb}";
            await Delete(uri, id, auth);
        }
        public async Task Delete(string uri, string id, string auth = "")
        {
            var requesturi = $"{uri}/{id}";
            await HttpRequestFactory.Delete(requesturi, auth);
        }

        public async Task<ProcessResultViewModel<TOut>> GetByUri(string requesturi, string auth = "")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOut>>();
            return result;
        }
        public async Task<ProcessResultViewModel<TOutCustomize>> GetByUriCustomized<TOutCustomize>(string requesturi, string auth = "")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<ProcessResultViewModel<TOutCustomize>>();
            return result;
        }

        public async Task<ProcessResultViewModel<List<TOut>>> GetListByUri(string requesturi, string auth = "")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<ProcessResultViewModel<List<TOut>>>();
            return result;
        }
        public async Task<ProcessResultViewModel<List<TOutCustomize>>> GetListByUriCustomize<TOutCustomize>(string requesturi, string auth = "")
        {
            HttpResponseMessage response = null;
            response = await HttpRequestFactory.Get(requesturi, auth);
            var result = response.ContentAsType<ProcessResultViewModel<List<TOutCustomize>>>();
            return result;
        }


    }
}
