﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  DispatchProduct.RepositoryModule
{
    public class ContextSeed
    {
        public async Task SeedEntityAsync<T>(DbContext context, List<T> seedDataEntities,bool saveChanges=true) where T : class
        {
            if (!context.Set<T>().Any())
            {
                DbSet<T> set = context.Set<T>();
                foreach (var entity in seedDataEntities)
                {
                    await set.AddAsync(entity);
                }
                if (saveChanges)
                {
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
