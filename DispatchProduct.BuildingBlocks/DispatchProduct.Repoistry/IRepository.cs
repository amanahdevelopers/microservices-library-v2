﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilites.PaginatedItems;
using Utilites.ProcessingResult;

namespace DispatchProduct.RepositoryModule
{
    public interface IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        ProcessResult<IQueryable<TEntity>> GetAllQuerable();
        ProcessResult<List<TEntity>> GetAll();
        ProcessResult<List<TEntity>> GetAll(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<TEntity> Get(params object[] id);
        ProcessResult<int> Count();
        ProcessResult<List<TEntity>> GetByIds(List<int> ids);
        ProcessResult<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        ProcessResult<TEntity> Add(TEntity entity);
        ProcessResult<List<TEntity>> Add(List<TEntity> entityLst);
        ProcessResult<bool> Update(TEntity entity);
        ProcessResult<bool> Update(List<TEntity> entityLst);
        ProcessResult<bool> Delete(TEntity entity);
        ProcessResult<bool> Delete(List<TEntity> entitylst);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated<TKey>(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate, Func<TEntity, TKey> orderDescExpr);
        ProcessResult<PaginatedItems<TEntity>> GetAllPaginated(PaginatedItems<TEntity> paginatedItems, Expression<Func<TEntity, bool>> predicate);
        ProcessResult<bool> DeleteById(params object[] id);
        int SaveChanges();
    }
}
