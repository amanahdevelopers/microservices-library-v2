﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace DispatchProduct.RepositoryModule
{
    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> CreateEqualSingleExpression<T>(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);
                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = False<T>();
                    foreach (var item in valueLst)
                    {
                        var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, ((JValue)item).Value) : Convert.ChangeType(((JValue)item).Value, propertyType);
                        //p.Property == value
                        var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(valueWithType));

                        //p => p.Property == value
                        var lambda = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                        predicate = predicate.Or(lambda);
                    }
                    result = predicate;
                }
                else
                {
                    var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, value) : Convert.ChangeType(value, propertyType);
                    //var valueWithType = Convert.ChangeType(value, propertyType);
                    var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(valueWithType));
                    //p => p.Property == value
                    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                }
            }
            return result;
        }

        public static Expression<Func<T, bool>> CreateNotEqualSingleExpression<T>(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);
                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = False<T>();
                    foreach (var item in valueLst)
                    {
                        var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, ((JValue)item).Value) : Convert.ChangeType(((JValue)item).Value, propertyType);
                        //p.Property == value
                        var equalsExpression = Expression.NotEqual(propertyExpression, Expression.Constant(valueWithType));

                        //p => p.Property == value
                        var lambda = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                        predicate = predicate.Or(lambda);
                    }
                    result = predicate;
                }
                else
                {
                    var valueWithType = propertyType.IsEnum ? Enum.ToObject(propertyType, value) : Convert.ChangeType(value, propertyType);
                    //var valueWithType = Convert.ChangeType(value, propertyType);
                    var equalsExpression = Expression.NotEqual(propertyExpression, Expression.Constant(valueWithType));
                    //p => p.Property == value
                    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                }
            }
            return result;
        }

        public static Expression<Func<T, bool>> CreateContainsExpression<T>(string property, string value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });

                var containsExpression = Expression.Call(propertyExpression, method, Expression.Constant(Convert.ChangeType(value, propertyType)));
                //p => p.Property == value
                result = Expression.Lambda<Func<T, bool>>(containsExpression, p);

            }
            return result;
        }
        public static Expression<Func<T, bool>> CreateContainsListExpression<T,T2>(string property, List<T2> value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                MethodInfo method = typeof(List<T2>).GetMethod("Contains", new[] { typeof(List<T2>) });

                var containsExpression = Expression.Call(propertyExpression, method, Expression.Constant(Convert.ChangeType(value, propertyType)));
                //p => p.Property == value
                result = Expression.Lambda<Func<T, bool>>(containsExpression, p);

            }
            return result;
        }

        public static Expression<Func<T, bool>> CreateGreaterThanOrLessThanSingleExpression<T>(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);
                string firstValue=null;
                string secondValue = null;
                if (value is List<string>)
                {
                    firstValue = ((List<string>)value).FirstOrDefault();
                    secondValue = ((List<string>)value).LastOrDefault();
                }
                else if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    firstValue = valueLst.First().ToString();
                    secondValue = valueLst.Skip(1).Take(1).First().ToString();
                }
                DateTime firstDate;
                bool firstBoolDate = false;
                bool secondBoolDate = false;
                DateTime secondDate;
                BinaryExpression greaterThanExpr = null;
                BinaryExpression lessThanExpr = null;
                if (DateTime.TryParse(firstValue.ToString(), out firstDate))
                {
                    firstBoolDate = true;

                }
                if (DateTime.TryParse(secondValue.ToString(), out secondDate))
                {
                    secondBoolDate = true;
                }
                if (firstBoolDate == secondBoolDate == true)
                {

                    TimeSpan duration = new System.TimeSpan(0, 23, 59, 59);
                    secondDate = secondDate.Date;
                    secondDate = secondDate.Add(duration);
                    firstDate = firstDate.Date;
                    greaterThanExpr = Expression.GreaterThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)firstDate.ToString()).Value, propertyType)));
                    lessThanExpr = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)secondDate.ToString()).Value, propertyType)));
                }
                else
                {
                    greaterThanExpr = Expression.GreaterThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)firstValue).Value, propertyType)));
                    lessThanExpr = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)secondValue).Value, propertyType)));
                }
                var predicate = PredicateBuilder.False<T>();
                var lambdaGreater = Expression.Lambda<Func<T, bool>>(greaterThanExpr, p);
                predicate = predicate.Or(lambdaGreater);
                var lambdaLess = Expression.Lambda<Func<T, bool>>(lessThanExpr, p);
                result = predicate.And(lambdaLess);

            }
            return result;
        }
    }
}