﻿using Newtonsoft.Json;
using ServiceReference1;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MakaniLocationService
{
    public static class MakaniHelper
    {
        public static async Task<MakaniModel> GetMakaniInfoByCoord(double latitude, double longitude)
        {
            try
            {
                MakaniPublicClient objClient = new MakaniPublicClient();
                var response = await objClient.GetMakaniInfoFromCoordAsync(latitude.ToString(), longitude.ToString(), "Public Remarks");
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response);
                string latLng = obj.MAKANI_INFO[0].LATLNG;
                MakaniModel makaniModel = new MakaniModel
                {
                    BLDG_NAME_A = obj.BLDG_NAME_E,
                    BLDG_NAME_E = obj.BLDG_NAME_E,
                    COMMUNITY_A = obj.COMMUNITY_A,
                    COMMUNITY_E = obj.COMMUNITY_E,
                    EMIRATE_A = obj.EMIRATE_A,
                    EMIRATE_E = obj.EMIRATE_E,
                    ENT_NAME_A = obj.MAKANI_INFO[0].ENT_NAME_A,
                    ENT_NAME_E = obj.MAKANI_INFO[0].ENT_NAME_E,
                    ENT_NO = obj.MAKANI_INFO[0].ENT_NO,
                    LAT = latLng.Split(',').ToList().First(),
                    LNG = latLng.Split(',').ToList().Skip(1).Take(1).First(),
                    MAKANI = obj.MAKANI_INFO[0].MAKANI,
                    SHORT_URL = obj.MAKANI_INFO[0].SHORT_URL
                };
                return makaniModel;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public static async Task<MakaniModel> GetMakaniInfoByNumber(string makaniNumber)
        {
            try
            {
                MakaniPublicClient objClient = new MakaniPublicClient();
                var response = await objClient.GetMakaniDetailsAsync(makaniNumber, "Public Remarks");
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response);
                string latLng = obj.MAKANI_INFO[0].LATLNG;

                MakaniModel makaniModel = new MakaniModel
                {
                    MAKANI = obj.MAKANI,
                    BLDG_NAME_A = obj.MAKANI_INFO[0].BLDG_NAME_A,
                    BLDG_NAME_E = obj.MAKANI_INFO[0].BLDG_NAME_E,
                    COMMUNITY_A = obj.MAKANI_INFO[0].COMMUNITY_A,
                    COMMUNITY_E = obj.MAKANI_INFO[0].COMMUNITY_E,
                    EMIRATE_A = obj.MAKANI_INFO[0].EMIRATE_A,
                    EMIRATE_E = obj.MAKANI_INFO[0].EMIRATE_E,
                    ENT_NAME_A = obj.MAKANI_INFO[0].ENT_NAME_A,
                    ENT_NAME_E = obj.MAKANI_INFO[0].ENT_NAME_E,
                    ENT_NO = obj.MAKANI_INFO[0].ENT_NO,
                    LAT = latLng.Split(',').ToList().First(),
                    LNG = latLng.Split(',').ToList().Skip(1).Take(1).First(),
                    SHORT_URL = obj.MAKANI_INFO[0].LATLNG
                };
                return makaniModel;
            }
            catch (Exception ex)
            {
                return null;
            }            
        }
    }
}

