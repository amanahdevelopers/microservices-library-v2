﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleAPI.Outputs
{
    public class ShowVehiclesRegDTO
    {
        public List<VehicleReg> vehiclesreg { get; set; }
    }
}
