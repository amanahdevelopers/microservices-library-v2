﻿using Microsoft.Spatial;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Demo
{
    
    public sealed class ClosestPoints
    {
        public ClosestPoints(GeographyPoint p1, GeographyPoint p2)
        {
            _p1 = p1;
            _p2 = p2;
        }

        public GeographyPoint P1 { get { return _p1; } }
        public GeographyPoint P2 { get { return _p2; } }

        public double? Distance()
        {
            return P1.Distance(P2);
        }

        private readonly GeographyPoint _p1;
        private readonly GeographyPoint _p2;
    }
}