﻿using Utilities.ProcessingResult;

namespace Utilites.ProcessingResult
{
    public interface IProcessResultStatus
    {
        ProcessResultStatusCode Code { get; set; }
        string Message { get; set; }
    }
}
