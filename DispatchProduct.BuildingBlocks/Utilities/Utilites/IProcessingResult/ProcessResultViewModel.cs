﻿namespace Utilites.ProcessingResult
{
    public interface IProcessResultViewModel<T>
    {
        bool IsSucceeded { get; set; }
        string MethodName { get; set; }
        ProcessResultStatus Status { get; set; }
        T Data { get; set; }
    }
}
