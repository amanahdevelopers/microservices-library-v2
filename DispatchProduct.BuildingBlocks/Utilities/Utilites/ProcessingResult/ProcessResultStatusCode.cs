﻿namespace Utilities.ProcessingResult
{
    public enum ProcessResultStatusCode
    {
        DefaultValue = 0,
        Succeeded = 1,
        Failed = 2,
        NotFound = 3,
        MissingArguments = 4,
        NullArguments = 5,
        NotSupportedExtension = 6,
        InvalidValue = 6
    }
}
