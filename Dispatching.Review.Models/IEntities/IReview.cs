﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dispatching.Reviewing.Models.Entities
{
    public interface IReview:IBaseEntity
    {
         int Id { get; set; }
         string FK_Reviewer_Id  { get; set; }
         string Fk_Reviewed_Id { get; set; }
         string Comment { get; set; }
         int FK_LKP_Item_Id { get; set; }
         int FK_LKP_Rating_Scale_Id { get; set; }
         LKP_Item LKP_Item { get; set; }
         LKP_Rating_Scale LKP_Rating_Scale { get; set; }
    }
}
